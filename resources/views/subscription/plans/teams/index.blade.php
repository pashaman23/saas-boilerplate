@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-md-offset-3">
                <ul class="list-group">
                    @foreach ($plans as $plan)
                        <li class="list-group-item">
                            <a href="{{ route('subscription.index') }}?plan={{ $plan->slug }}">{{ $plan->name }} (£{{ $plan->price }})</a>
                        </li>
                    @endforeach

                    <li class="list-group-item">
                        <a href="{{ route('plans.index') }}">User plans</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
