@component('mail::message')

Dear {{ $user->name }}, your password was changed.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
