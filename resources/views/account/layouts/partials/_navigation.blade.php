<div class="list-group">
    <a href="{{ route('account.index') }}" class="list-group-item list-group-item-action {{ return_if(on_page('account'), 'active') }}">
        Account overview
    </a>
    <a href="{{ route('account.profile.index') }}" class="list-group-item list-group-item-action {{ return_if(on_page('*/profile'), 'active') }}">Profile</a>
    <a href="{{ route('account.password.index') }}" class="list-group-item list-group-item-action {{ return_if(on_page('*/password'), 'active') }}">Change password</a>
    <a href="{{ route('account.deactivate.index') }}" class="list-group-item list-group-item-action {{ return_if(on_page('*/deactivate'), 'active') }}">Deactivate account</a>
</div>

<hr>

<div class="list-group">
    <a href="{{ route('account.files.create') }}" class="list-group-item list-group-item-action {{ return_if(on_page('*/files/create'), 'active') }}">
        Add file
    </a>
    <a href="{{ route('account.files.index') }}" class="list-group-item list-group-item-action {{ return_if(on_page('*/files'), 'active') }}">
        Files
    </a>
</div>

@subscribed
<hr>
<div class="list-group">
    @subscriptionnotcancelled
        <a href="{{ route('account.subscription.cancel.index') }}" class="list-group-item list-group-item-action {{ return_if(on_page('account/subscription/cancel'), 'active') }}">Cancel subscription</a>
    @endsubscriptionnotcancelled

    @subscriptioncancelled
    <a href="{{ route('account.subscription.resume.index') }}" class="list-group-item list-group-item-action {{ return_if(on_page('account/subscription/resume'), 'active') }}">Resume subscription</a>
    @endsubscriptioncancelled
    <a href="{{ route('account.subscription.card.index') }}" class="list-group-item list-group-item-action {{ return_if(on_page('account/subscription/card'), 'active') }}">Update card</a>

</div>
@endsubscribed
