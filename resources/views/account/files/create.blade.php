@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ route('account.files.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="title" class="control-label">Title</label>
                            <input type="text" name="title" id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title') }}">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="uploads" class="control-label">Please upload at least one file</label>
                            <input type="file" name="uploads[]" id="uploads" class="form-control{{ $errors->has('uploads') ? ' is-invalid' : '' }}" value="" multiple>

                            @if ($errors->has('uploads'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('uploads') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
