@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach($files as $file)
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="{{ route('account.files.show', ['file' => $file]) }}">
                                    {{ $file->title }}
                                </a>
                                <span class="badge badge-primary badge-pill"><a href="{{ route('account.files.delete', ['file' => $file]) }}" class="text-light bg-dark">Delete</a></span>
                                <span class="badge badge-primary badge-pill">{{ $file->uploads_count }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
