@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="list-group">
                        <p class="h2">{{ $file->title }}</p>
                        @foreach($file->uploads as $upload)
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="{{ route('account.files.download', ['upload' => $upload]) }}">
                                    {{ $upload->filename }}
                                </a>
                                <img src="{{ upload_path($file->id, $upload->filename) }}" alt="" style="width: 50px; height: 50px;">
                                {{--<span class="badge badge-primary badge-pill"><a href="{{ route('account.files.delete', ['file' => $file]) }}" class="text-light bg-dark">Delete</a></span>--}}
                                {{--<span class="badge badge-primary badge-pill">{{ $file->uploads_count }}</span>--}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
