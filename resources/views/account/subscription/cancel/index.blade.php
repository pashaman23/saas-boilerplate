@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ route('account.subscription.cancel.store') }}" method="POST">
                        @csrf

                        <p>Cancel subscription.</p>

                        <button type="submit" class="btn btn-primary">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
