@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ route('account.subscription.resume.store') }}" method="POST">
                        @csrf

                        <p>Resume subscription.</p>

                        <button type="submit" class="btn btn-primary">Resume</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
