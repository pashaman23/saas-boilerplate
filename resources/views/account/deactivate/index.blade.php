@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ route('account.deactivate.store') }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <label for="name" class="control-label">Confirm password</label>
                            <input type="password" name="password_current" id="password_current" class="form-control{{ $errors->has('password_current') ? ' is-invalid' : '' }}" value="">

                            @if ($errors->has('password_current'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_current') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Deactivate</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
