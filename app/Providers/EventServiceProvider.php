<?php

namespace App\Providers;

use App\Events\Auth\UserSignedUp;
use App\Events\PasswordChanged;
use App\Events\UserRequestedActivationEmail;
use App\Listeners\Auth\SendActivationEmail;
use App\Listeners\UserChangedPassword;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        PasswordChanged::class => [
          UserChangedPassword::class,
        ],
        UserSignedUp::class => [
            SendActivationEmail::class,
        ],
        UserRequestedActivationEmail::class => [
            SendActivationEmail::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
