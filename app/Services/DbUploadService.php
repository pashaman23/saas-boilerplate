<?php

namespace App\Services;

use App\Models\File;
use App\Models\Upload;
use Illuminate\Http\UploadedFile;

class DbUploadService
{
    public function saveUploadToDb(UploadedFile $uploadedFile, File $file)
    {
        $upload = new Upload();
        $upload->filename = $uploadedFile->getClientOriginalName();
        $upload->size = $uploadedFile->getSize();
        $upload->file()->associate($file);
        $upload->user()->associate(auth()->user());
        $upload->save();

        return $upload;
    }
}
