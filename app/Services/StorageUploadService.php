<?php

namespace App\Services;

use App\Models\File;
use Illuminate\Http\UploadedFile;
use Storage;

class StorageUploadService
{
    public function saveUploadOnDisk(File $file, UploadedFile $uploadedFile)
    {
        Storage::disk('public')->putFileAs(
            $file->id,
            $uploadedFile,
            $uploadedFile->getClientOriginalName()
        );
    }

    public function deleteUploadFromDisk(File $file)
    {
        Storage::deleteDirectory('public/' . $file->id);
    }
}
