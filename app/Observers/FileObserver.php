<?php

namespace App\Observers;

use App\Models\File;
use App\Services\StorageUploadService;

class FileObserver
{
    public $uploadService;

    public function __construct(StorageUploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    public function deleting(File $file)
    {
        $file->uploads()->delete();
        $this->uploadService->deleteUploadFromDisk($file);
    }
}
