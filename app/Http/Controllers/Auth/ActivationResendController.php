<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRequestedActivationEmail;
use App\Http\Requests\ActivationResendStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\User;

class ActivationResendController extends Controller
{
    public function index()
    {
        return view('auth.activation.resend.index');
    }

    public function store(ActivationResendStoreRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        event(new UserRequestedActivationEmail($user));

        return redirect()->route('login')->with('success', 'An activation email has been sent.');
    }
}
