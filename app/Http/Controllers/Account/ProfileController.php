<?php

namespace App\Http\Controllers\Account;

use App\Http\Requests\ProfileStoreRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index()
    {
        return view('account.profile.index');
    }

    public function store(ProfileStoreRequest $request)
    {
        auth()->user()->update($request->only('name', 'email'));

        return back()->with('success', 'Account details updated.');
    }
}
