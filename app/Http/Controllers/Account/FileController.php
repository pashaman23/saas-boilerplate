<?php

namespace App\Http\Controllers\Account;

use App\Http\Requests\FileStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Upload;
use App\Services\DbUploadService;
use App\Services\StorageUploadService;

class FileController extends Controller
{
    public $dbUploadService;
    public $storageUploadService;

    public function __construct(DbUploadService $dbUploadService, StorageUploadService $storageUploadService)
    {
        $this->dbUploadService = $dbUploadService;
        $this->storageUploadService = $storageUploadService;
    }

    public function index()
    {
        $files = File::where('user_id', auth()->id())->withCount('uploads')->get();

        return view('account.files.index', compact('files'));
    }

    public function create()
    {
        return view('account.files.create');
    }

    public function show(File $file)
    {
        $this->authorize('touch', $file);

        $file->load('uploads');

        return view('account.files.show', compact('file'));
    }

    public function store(FileStoreRequest $request)
    {
        $file = File::create([
            'title' => $request->title,
            'user_id' => auth()->id(),
        ]);

        $uploadedFiles = $request->uploads;

        foreach ($uploadedFiles as $uploadedFile) {
            $this->dbUploadService->saveUploadToDb($uploadedFile, $file);
            $this->storageUploadService->saveUploadOnDisk($file, $uploadedFile);
        }

        return redirect()->route('account.index')->with('success', 'File has been created');

    }

    public function destroy(File $file)
    {
        $this->authorize('touch', $file);

        $file->delete();

        return back()->with('success', 'File has been deleted');
    }

    public function getDownload(Upload $upload)
    {
        return \Storage::download('public/'. $upload->file->id . '/' . $upload->filename);
    }
}
