<?php

namespace App\Http\Controllers\Account;

use App\Events\PasswordChanged;
use App\Http\Requests\PasswordStoreRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordController extends Controller
{
    public function index()
    {
        return view('account.password.index');
    }

    public function store(PasswordStoreRequest $request)
    {
        $user = auth()->user();
        $user->changePassword($request->password);

        event(new PasswordChanged($user));

        return back()->with('success', 'Password updated.');
    }
}
