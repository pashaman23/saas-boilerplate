<?php

namespace App\Http\Middleware;

use Closure;

class CheckExpiredConfirmationToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $redirect)
    {
        if ($request->confirmationToken->hasExpired()) {
            return redirect($redirect)->with('error', 'Token expired.');
        }

        return $next($request);
    }
}
