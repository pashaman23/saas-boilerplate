<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User();
        $user->name = 'Test';
        $user->email = 'test@gmail.com';
        $user->email_verified_at = null;
        $user->password = \Illuminate\Support\Facades\Hash::make('123456789');
        $user->remember_token = null;
        $user->activated = true;
        $user->save();
    }
}
