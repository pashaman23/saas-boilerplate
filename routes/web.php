<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('dashboard', 'DashboardController@index')->name('dashboard.index')->middleware(['auth']);

Route::group(['prefix' => 'account', 'as' => 'account.', 'namespace' => 'Account', 'middleware' => 'auth'], function () {
    Route::get('/', 'AccountController@index')->name('index');

    Route::get('profile', 'ProfileController@index')->name('profile.index');
    Route::post('profile', 'ProfileController@store')->name('profile.store');

    Route::get('password', 'PasswordController@index')->name('password.index');
    Route::post('password', 'PasswordController@store')->name('password.store');

    Route::get('deactivate', 'DeactivateController@index')->name('deactivate.index');
    Route::post('deactivate', 'DeactivateController@store')->name('deactivate.store');

    Route::group(['prefix' => 'files', 'as' => 'files.'], function () {
        Route::get('/', 'FileController@index')->name('index');
        Route::get('create', 'FileController@create')->name('create');
        Route::get('{file}/show', 'FileController@show')->name('show');
        Route::post('store', 'FileController@store')->name('store');
        Route::get('{file}/delete', 'FileController@destroy')->name('delete');
        Route::get('{upload}/download', 'FileController@getDownload')->name('download');
    });

    Route::group(['prefix' => 'subscription', 'namespace' => 'Subscription', 'as' => 'subscription.'], function () {
        Route::group(['middleware' => 'subscription.notcancelled'], function () {
            Route::get('cancel', 'SubscriptionCancelController@index')->name('cancel.index');
            Route::post('cancel', 'SubscriptionCancelController@store')->name('cancel.store');
        });

        Route::group(['middleware' => 'subscription.cancelled'], function () {
            Route::get('resume', 'SubscriptionResumeController@index')->name('resume.index');
            Route::post('resume', 'SubscriptionResumeController@store')->name('resume.store');
        });

        Route::group(['middleware' => 'subscription.customer'], function () {
            Route::get('card', 'SubscriptionCardController@index')->name('card.index');
            Route::post('card', 'SubscriptionCardController@store')->name('card.store');
        });
    });
});

Route::group(['prefix' => 'activation', 'as' => 'activation.', 'middleware' => ['guest'], 'namespace' => 'Auth'], function () {
    Route::get('/resend', 'ActivationResendController@index')->name('resend');
    Route::post('/resend', 'ActivationResendController@store')->name('resend.store');
    Route::get('/{confirmationToken}', 'ActivationController@activate')->name('activate')->middleware(['confirmation_token.expired:/']);
});

Route::group(['prefix' => 'plans', 'as' => 'plans.', 'namespace' => 'Subscription', 'middleware' => 'subscription.inactive'], function () {
    Route::get('/', 'PlanController@index')->name('index');
    Route::get('/teams', 'PlanTeamController@index')->name('teams.index');
});

Route::group(['prefix' => 'subscription', 'as' => 'subscription.', 'namespace' => 'Subscription', 'middleware' => ['auth.register', 'subscription.inactive']], function () {
    Route::get('/', 'SubscriptionController@index')->name('index');
    Route::post('/', 'SubscriptionController@store')->name('store');
});
